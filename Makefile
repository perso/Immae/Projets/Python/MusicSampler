install:
	poetry install

build:
	msgfmt music_sampler/locales/fr/LC_MESSAGES/music_sampler.po -o music_sampler/locales/fr/LC_MESSAGES/music_sampler.mo

run: install build
	poetry run python run.py

BUNDLER ?= shiv
bundle: install build
	$(MAKE) bundle-$(BUNDLER)

bundle-shiv:
	if [ -f music_sampler.pyz ]; then rm music_sampler.pyz; fi
	git add -f music_sampler/locales/fr/LC_MESSAGES/music_sampler.mo
	poetry run shiv --output-file=music_sampler.pyz --site-packages=$(shell poetry env info -p)/lib/python3.9/site-packages --python="/usr/bin/env python3" --entry-point=music_sampler.app.main --no-deps ./.
	git restore --staged music_sampler/locales/fr/LC_MESSAGES/music_sampler.mo

bundle-pyinstaller:
	poetry run pyinstaller music_sampler.spec

PACKAGER ?= poetry

package: install build
	git describe --exact-match --tags
	$(MAKE) package-$(PACKAGER)

package-setup:
	poetry run python setup.py sdist

package-poetry:
	poetry version $(shell git describe --exact-match --tags)
	poetry build

publish: package publish-$(PACKAGER)

publish-setup:
	poetry run python setup.py upload

publish-poetry:
	poetry publish

dump-readme:
	ssh gitolite@git.immae.eu readme perso/Immae/Projets/Python/MusicSampler

update-readme:
	poetry run python build_readme.py | ssh gitolite@git.immae.eu readme perso/Immae/Projets/Python/MusicSampler set
